const display = document.querySelector("#display");
const buttons = document.querySelectorAll(".button");

let state;

function roundDecPl(n, p) {
    const a = Math.pow(10, p);
    return Math.round(n * a) / a;
}

// calc*() and operate() are requirements in the exercise.
function calcAdd(a, b) {
    return a + b;
}
function calcSubtract(a, b) {
    return a - b;
}
function calcMultiply(a, b) {
    return a * b;
}
function calcDivide(a, b) {
    return a / b;
}
function operate(a, b, op) {
    switch (op) {
        case "add":      return calcAdd(a, b);
        case "subtract": return calcSubtract(a, b);
        case "multiply": return calcMultiply(a, b);
        case "divide":   return calcDivide(a, b);
        default:
            throw "Invalid operation.";
    }
}

function parseCurrStr(v) {
    if (v == "" || v == "-") return 0;
    return parseFloat(v);
}

// This probably doesn't cover all edge cases, but good enough for a simple exercise
function toDisplayablePrev(v) {
    if (v == 0) return "0";

    const magnitude = Math.floor(Math.log10(Math.abs(v)));
    if (magnitude > 9 || magnitude < -10) {
        const smallestStr = v.toExponential(0);
        if (smallestStr.length > 12) return "too long";
        return v.toExponential(12 - smallestStr.length);
    } else {
        const s = roundDecPl(v, 12 - Math.round(v).toFixed(0).length);
        return s.toFixed(12 - s.toFixed(0).length);
    }
}

function updateDisplay() {
    const s = (state.mode == "show-prev") ? toDisplayablePrev(state.prev)
            : (state.mode == "show-curr") ? state.curr
            : (state.mode == "error")     ? "&macr;\\_(&#12484;)_/&macr;"
            : "Unknown error.";
    display.innerHTML = (s == "") ? "0" : s;
}

function reset() {
    state = {
        curr: "", // String
        prev: 0, // Number
        op: "add", // "add" | "subtract" | "multiply" | "divide"
        mode: "show-prev", // "show-prev" | "show-curr" | "error"
    }
    updateDisplay();
}

function doInput(v) {
    if (v == "clear") {
        reset();
        return;
    } else if (state.mode == "error") {
        return;
    }
    console.log(state);

    switch (v) {
        case "delete":
            state.curr = state.curr.slice(0, -1);
            state.mode = "show-curr";
            break;
        case "equals":
            state.prev = operate(state.prev, parseCurrStr(state.curr), state.op);
            state.curr = "";
            state.op = "add";
            state.mode = "show-prev";
            break;
        case "subtract":
            if (state.mode == "show-prev") {
                state.curr = "-";
                state.mode = "show-curr";
                break;
            }
        case "add":
        case "multiply":
        case "divide":
            state.prev = operate(state.prev, parseCurrStr(state.curr), state.op);
            state.op = v;
            state.mode = "show-prev";
            break;
        case ".":
            if (state.mode == "show-curr" && state.curr.includes(".")) break;
        default: // Assume to be a digit
            if (state.mode == "show-curr" && state.curr.length == 13) break;
            if (v == "0" && (state.curr == "0" || state.curr == "-")) break;
            state.curr = (state.mode == "show-prev") ? v : state.curr.concat(v);
            state.mode = "show-curr";
    }

    if (!isFinite(state.prev)) {
        state.mode = "error";
    }
    updateDisplay();
}

buttons.forEach(elem => {
    elem.addEventListener("click", e => {
        doInput(e.target.dataset.value);
    })
});

document.addEventListener("keydown", e => {
    if (e.key.match(/^[0-9]$/)) {
        doInput(e.key);
    }

    switch (e.key) {
        case ".":
            doInput(".");
            break;
        case "+":
            doInput("add");
            break;
        case "-":
            doInput("subtract");
            break;
        case "*":
        case "x":
            doInput("multiply");
            break;
        case "/":
        case "\\":
            doInput("divide");
            break;
        case "=":
        case "Enter":
            doInput("equals");
            break;
        case "Escape":
            doInput("clear");
            break;
        case "z":
            if (!e.ctrlKey) break;
        case "Backspace":
            doInput("delete");
            break;
        default:
            // Ignore
    }
});

reset();

